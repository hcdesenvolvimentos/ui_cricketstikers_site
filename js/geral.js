$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
    setTimeout(function(){  
    	$("#carrosselDepoimentos").owlCarousel({
            items : 4,
            dots: true,
            loop: false,
            lazyLoad: true,
            mouseDrag:true,
            touchDrag  : true,	       
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            smartSpeed: 450,
    	    //CARROSSEL RESPONSIVO
    	    responsiveClass:true,			    
            responsive:{
                425:{
                    items:1
                },
                600:{
                    items:2
                },
               
                991:{
                    items:3
                },
                1024:{
                    items:4
                },
                1440:{
                    items:5
                },
                			            
            }		    		   		    
        	    
        	});
    }, 4000);

    //CARROSSEL DE DESTAQUE
    $("#carrosselTeam").owlCarousel({
        items : 5,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,             
            responsive:{
                320:{
                    items:3
                },
                600:{
                    items:5
                },
               
                991:{
                    items:5
                },
                1024:{
                    items:5
                },
                1440:{
                    items:5
                },
                                        
            }                                
            
        });

    //*********************
    	//APP
    //*********************
    $(".meettheTeam .carrosselTeam .item").click(function(e){
  		let dataUrl  = $(this).attr("dataUrl");
  		let dataDescription  = $(this).attr("dataDescription");
  		let dataName  = $(this).attr("dataName");
  		let dataFunction  = $(this).attr("dataFunction");
  
  		$("#imgTeam").attr("src", dataUrl);
  		$("#description").text(dataDescription);
  		$("#name").text(dataName);
  		$("#function").text(dataFunction);

	});
    setTimeout(function(){  
     $(".item.active").trigger("click");
    }, 2000);
  
});